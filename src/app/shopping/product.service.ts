import { Injectable } from '@angular/core';

import { Observable, of } from 'rxjs';

import { Product } from '../models';
import { products } from './mock-product-db';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor() { }

  getProducts(): Observable<Product[]> {
    return of(products as Product[]);
  }
}
