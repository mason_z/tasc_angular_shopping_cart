import { Product } from "../models";

export const products: Product[] = [
    {
        name: '16lb bag of Skittles',
        price: 16.00,
        category: 'candy',
        imported: false
    },
    {
        name: 'Walkman',
        price: 99.99,
        category: 'electronic',
        imported: false
    },
    {
        name: 'Bag of microwave popcorn',
        price: 0.99,
        category: 'popcorn',
        imported: false
    },
    {
        name: 'Bag of Vanilla-Hazelnut Coffee',
        price: 11.00,
        category: 'coffee',
        imported: true
    },
    {
        name: 'Vespa',
        price: 15001.25,
        category: 'motor bike',
        imported: true
    },
    {
        name: 'Crate of Almond Snickers',
        price: 75.99,
        category: 'candy',
        imported: true
    },
    {
        name: 'Discman',
        price: 55.00,
        category: 'electronic',
        imported: false
    },
    {
        name: 'Bottle of Wine',
        price: 10,
        category: 'motor bike',
        imported: true
    },
    {
        name: '300lb bag of Fair-Trade Coffee',
        price: 997.99,
        category: 'coffee',
        imported: false
    },
]