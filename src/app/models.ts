export interface Product {
    name: string,
    price: number,
    category: string,
    imported: boolean
}

export interface CartItemForCheckout {
    productName: string,
    quantity: number,
    priceBeforeTax: number,
    itemTax?: number
}

export interface CartItem {
    product: Product,
    quantity: number
}