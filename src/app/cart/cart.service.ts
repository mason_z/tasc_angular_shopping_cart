import { Injectable } from '@angular/core';

import { BehaviorSubject } from 'rxjs';

import { CartItem, Product } from '../models';

@Injectable({
  providedIn: 'root'
})
export class CartService {
  private _cart$ = new BehaviorSubject<Map<string, CartItem>>(new Map());

  public cart$ = this._cart$.asObservable();

  constructor() { }

  addToCart(addedProduct: Product) {
    let cart = this._cart$.getValue();
    
    if (!cart.has(addedProduct.name)) {
        cart.set(addedProduct.name, {
          product: addedProduct,
          quantity: 1
        })
    } else {
        let cartItem = cart.get(addedProduct.name);
        cartItem.quantity += 1;
        cart.set(addedProduct.name, cartItem);
      }

      this._cart$.next(cart);
  }

  clearCart() {
    this._cart$.next(new Map());
  }
}
