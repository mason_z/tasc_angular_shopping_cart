import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { MatDividerModule } from '@angular/material/divider';

import { CheckoutComponent } from './checkout/checkout.component';
import { CartComponent } from './cart/cart.component';
import { CartlistComponent } from './cartlist/cartlist.component';

@NgModule({
  declarations: [
    CheckoutComponent,
    CartComponent,
    CartlistComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    MatDividerModule
  ],
  exports: [
    CartComponent
  ]
})
export class CartModule { }
