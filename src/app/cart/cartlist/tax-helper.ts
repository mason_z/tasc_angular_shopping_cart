export class TaxHelper {
    static baseRate = 0.10;
    static importRate = TaxHelper.baseRate + 0.05;
    static exemptionCategories = [
        "candy",
        "popcorn",
        "coffee"
    ]

    static calculateTaxOnItem(price: number, isImport: boolean, category: string): number {
        const isExempt = this.exemptionCategories.includes(category);

        if (isExempt) {
            return 0;
        }

        let tax: number;

        if (isImport) {
            tax = price * this.importRate;
        } else if (!isExempt) {
            tax = price * this.baseRate;
        } 

        return +((Math.ceil(tax * 20) / 20).toFixed(2));
    }

}