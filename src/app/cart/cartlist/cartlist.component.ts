import { Component, Input, OnDestroy, OnInit } from '@angular/core';

import { Subject, Observable } from 'rxjs';
import { takeUntil, map, tap } from 'rxjs/operators';

import { CartItemForCheckout } from '../../models';
import { CartService } from '../cart.service';
import { TaxHelper } from './tax-helper';

@Component({
  selector: 'app-cartlist',
  templateUrl: './cartlist.component.html',
  styleUrls: ['./cartlist.component.scss']
})
export class CartlistComponent implements OnInit, OnDestroy {
  @Input() applyTaxToPrice: boolean;
  @Input() includeTotal: boolean;
  
  private destroy$ = new Subject<boolean>();

  cartItems$: Observable<CartItemForCheckout[]>;
  totalBeforeTax: number;
  totalTax: number;
  totalPrice: number;

  constructor(public cartService: CartService) { }

  ngOnInit(): void {
    this.zeroTotals();

    this.cartItems$ = this.cartService.cart$
      .pipe(
        takeUntil(this.destroy$),
        map(cartItemMap => Array.from(cartItemMap.values())),
        tap(n => {
          if (!n.length) {
            this.zeroTotals();
          }
        }),
        map(cartItems => 
          cartItems.map(item => {
            const itemPriceBeforeTax = +(item.quantity * item.product.price).toFixed(2);
            const itemTax = TaxHelper.calculateTaxOnItem(itemPriceBeforeTax, item.product.imported, item.product.category)
            
            this.totalBeforeTax += +(item.quantity * item.product.price).toFixed(2);
            this.totalTax = +(this.totalTax += itemTax).toFixed(2);
            this.totalPrice = +(this.totalPrice += itemPriceBeforeTax + itemTax).toFixed(2);

            return {
              productName: item.product.name,
              quantity: item.quantity,
              priceBeforeTax: itemPriceBeforeTax,
              itemTax: itemTax
            }
          })
        )
      );
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  private zeroTotals() {
    this.totalBeforeTax = 0;
    this.totalTax = 0;
    this.totalPrice = 0;
  }
}

