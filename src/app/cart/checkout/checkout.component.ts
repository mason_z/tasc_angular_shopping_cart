import { Component} from '@angular/core';

import { CartService } from '../cart.service';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.scss']
})
export class CheckoutComponent {

  constructor(public cartService: CartService) { }

  purchase(): void {
    alert('Purchase successful!');
    this.cartService.clearCart();
  }

}
